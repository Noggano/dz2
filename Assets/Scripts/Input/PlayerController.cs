using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRigitBody;
    private Vector3 startPosition;

    [SerializeField]
    private float moveSpeed = 600f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerRigitBody = GetComponent<Rigidbody>();
        startPosition = transform.position;

        playerInputActions.Kart.ResetPosition.performed += ContextMenu => ResetPosition();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);
    }
    private void Move(Vector2 direction)
    {
        playerRigitBody.velocity = new Vector3(direction.x * moveSpeed * Time.fixedDeltaTime, 0f, direction.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void Update()
    {
        if (playerInputActions.Kart.Velosity.triggered)
        {
            if (moveSpeed == 600f)
            {
                moveSpeed = 1000f;
            }
            else
                moveSpeed = 600f;
        }
    }

    private void ResetPosition()
    {
        playerRigitBody.MovePosition(startPosition);
        playerRigitBody.MoveRotation(Quaternion.identity);
    }
    
}
